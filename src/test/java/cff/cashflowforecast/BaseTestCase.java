package cff.cashflowforecast;

import org.junit.jupiter.api.BeforeEach;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class BaseTestCase {
	@BeforeEach
	public void test() {
		MockitoAnnotations.initMocks(this);
		
	}
}
